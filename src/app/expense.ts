import{User} from './user';

export class Expense{
  paidBy: User;
  expense:string;
  amount:number;
}
