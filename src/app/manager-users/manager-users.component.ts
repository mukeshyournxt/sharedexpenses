import { Component, OnInit,Input } from '@angular/core';
import{User} from '../user';

@Component({
  selector: 'app-manager-users',
  templateUrl: './manager-users.component.html',
  styleUrls: ['./manager-users.component.css']
})
export class ManagerUsersComponent implements OnInit {

  @Input('users')
  users: User[];

  idSequence: number=5;
  tempUser: User={
    id:this.idSequence,name:'',mail:''
  };



  constructor() { }

  ngOnInit() {
    // this.userForm = new FormGroup({
    //   'name': new FormControl(this.hero.name, [
    //     Validators.required,
    //     Validators.minLength(4),
    //     forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
    //   ]),
    //   'alterEgo': new FormControl(this.hero.alterEgo),
    //   'power': new FormControl(this.hero.power, Validators.required)
    // });
  }

  deleteUser(user: User): void{
    var index = this.users.indexOf(user);
    this.users.splice(index,1);
  }

  addUser():void{
    this.users.push(this.tempUser);
    this.idSequence++;
    this.tempUser = new User();
  }

}
