import { Component, OnInit,Input } from '@angular/core';
import{User} from '../user';
import{Expense} from '../expense';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {

  @Input('users')
  users: User[];

  @Input('expenses')
  expenses: Expense[];


  tempExpense: Expense={
    paidBy: null,expense:null,amount:null
  };

  constructor() { }

  ngOnInit() {
  }


  deleteExpense(expense: Expense): void{
    var index = this.expenses.indexOf(expense);
    this.expenses.splice(index,1);
  }

  addExpense():void{
    this.expenses.push(this.tempExpense);
    this.tempExpense = new Expense();
  }
}
