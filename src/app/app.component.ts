import { Component } from '@angular/core';
import{User} from './user';
import{Expense} from './expense';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Roommate Expenses Sharing';
  public showUser:boolean = true;
  public showExpense:boolean = false;
  public showReport:boolean = false;

  users:User[]=[];

  expenses:Expense[]=[];


  showUserf():void{
    this.showUser=true;
    this.showExpense=false;
    this.showReport=false;
  }

  showExpensef():void{
    this.showUser=false;
    this.showExpense=true;
    this.showReport=false;
  }

  showReportf():void{
    this.showUser=false;
    this.showExpense=false;
    this.showReport=true;
  }
}
